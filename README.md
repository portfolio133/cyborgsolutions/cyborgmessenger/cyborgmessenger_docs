# CyborgMessenger_docs
This repository is created solely for documentation and managing requirements for all projects that will be developed in other small repositories of this project.

## Description

The "CyborgMessenger" project is an advanced internet messenger designed using various technologies to provide a comprehensive and intuitive platform for user communication. Below, we outline the key aspects of this project.

## Contributors
The project is created as part of the virtual company CyborgSolutions, where we aggregate the best IT specialists.  
Below is a list of contributors who have played a key role in advancing the application with short description:
1. [Andrzej Połetek](https://gitlab.poletek.net) - As a CEO, techlead, and programmer
2. [Ewa Kucała](https://github.com/Ewa-Anna) - Backend Developer

## Requirements
### Functional Requirements:
#### User Registration:
Users can create new accounts by providing basic information such as username, password, and email address.

#### Login:
Registered users can log in using their username and password.

#### User Profile:
Each user has a profile where they can manage personal information, avatars, and other settings.

#### Contact List:
Users can add other users to their contact list.

#### Message Exchange:
Users can send real-time text messages to their contacts.

#### Group Chats:
Enable the creation of group chats where users can communicate with multiple people simultaneously.

#### Notifications:
Notification system for new messages, friend requests, and other important events.

#### Attachments and Multimedia:
Ability to send and receive files, photos, and other multimedia within messages.

#### Conversation History:
Users have access to the history of their conversations, including message archives.

#### Emoji and Emoticons:
Support for emojis and emoticons in message content.

#### Online Status:
Visibility of online/invisible/offline status for users.

#### Multi-Device Support:
Ability to use the application on different platforms, such as web browsers, mobile applications on Android and iOS.

### Non-functional Requirements:
#### Performance:
Minimal server response time for basic operations.

#### Scalability:
The system should be easily scalable to handle a growing number of users.

#### Security:
Encrypted connections, protection against SQL Injection, and Cross-Site Scripting (XSS) attacks.

#### Availability:
High availability of the service with minimal maintenance downtime.

#### Browser Compatibility:
Proper functioning in popular web browsers.

#### Error Handling:
Clear error messages for users and logs for administrators.

#### Data Backup:
Regular backup of user data.

#### Integration with External Systems:
Integration with payment systems for premium features, social login services, etc.

#### System Monitoring:
Performance and availability monitoring system.

#### Compliance with Privacy Regulations:
Compliance with legal regulations regarding user data privacy.

#### Extensibility:
The project should be flexible and easily expandable with new features in the future.

#### Testing:
Implementation of a comprehensive testing system, including unit, integration, and acceptance testing.

## Architecture
### Databases
#### PostgreSQL
PostgreSQL will serve as the primary relational database for structured data storage.
It provides reliability, ACID compliance, and is suitable for various data types and complex queries.
#### MongoDB
MongoDB will be used as a NoSQL database, offering flexibility in handling unstructured or semi-structured data.
It can store data such as chat logs, historical records, or any data that fits a document-oriented model.
#### Redis
Redis may be employed for caching and handling real-time messaging in scenarios where fast data access is crucial.
It can be used to enhance performance and scalability, especially for features like notifications.
### Backend Layer
#### Django
Django will serve as the backend framework, providing a robust and scalable foundation for web application development.
It follows the model-view-controller (MVC) architectural pattern and comes with built-in features for authentication, ORM, and more.
#### Django-Rest-Framework
Django-Rest-Framework is an extension for Django that simplifies the creation of RESTful APIs.
It facilitates the development of API endpoints, serialization, authentication, and permissions.
#### Django-Channels
Django-Channels extends Django to handle WebSockets, enabling real-time bidirectional communication.
It is suitable for implementing features like chat, notifications, and other real-time interactions.

### Frontend Layer
#### React
React will be used for building the web frontend, providing a component-based architecture for efficient UI development.
It allows for the creation of a dynamic and interactive user interface.

#### Redux
Redux will be used for manage state of application and components

#### React Native
React Native will be utilized for developing the mobile frontend, allowing code sharing between web and mobile platforms.
It enables the creation of native mobile applications using React and JavaScript.

### Communication Between Layers
#### RESTful API
The RESTful API will be the primary communication protocol between the frontend and backend layers.
It follows REST principles, utilizing standard HTTP methods for communication.
#### WebSockets
WebSockets will be employed for real-time communication, providing low-latency bidirectional data flow.
This is crucial for features such as instant messaging, notifications, and live updates.
#### JSON Web Tokens
JSON Web Tokens (JWT) ensure secure information exchange, serving as a stateless and scalable solution for authentication in "CyborgMessenger." They securely transmit user data and enhance authentication security.

### Testing
#### Pytest
Pytest is a testing framework for Python, suitable for testing backend components and functionalities.
It simplifies test writing and provides detailed test reports.
#### Jest
Jest is a testing framework for JavaScript, commonly used for testing React and React Native applications.
It supports snapshot testing, mocking, and asynchronous testing.
#### Selenium
Selenium can be used for end-to-end testing, automating browser interactions to ensure the application functions correctly from a user perspective.
It supports various programming languages, including Python and JavaScript.


### DevOps
#### Docker and Docker-Compose
Docker and Docker-Compose will be utilized for containerization, allowing the deployment of applications and dependencies in isolated environments.
It simplifies the development and deployment processes, ensuring consistency across different environments.

#### Gitlab CI/CD
GitLab CI/CD (Continuous Integration/Continuous Deployment) will automate the testing and deployment processes.
It facilitates automatic testing upon code changes and streamlines the deployment of application updates.
#### Kubernetes
Kubernetes can be employed for container orchestration, automating the deployment, scaling, and management of containerized applications.
It enhances scalability, availability, and simplifies the deployment process.
#### Ansible IAAC
Ansible Infrastructure as Code (IAAC) can be used for automating infrastructure provisioning and configuration management.
It enables consistent and reproducible infrastructure deployments.

### Monitoring
#### Sentry
Sentry is an open-source error tracking and monitoring platform.
It helps identify and resolve issues by providing detailed error reports, logs, and insights into application performance.
#### ELK Stack
The ELK Stack (Elasticsearch, Logstash, Kibana) can be employed for log management, analysis, and visualization.
Elasticsearch stores and indexes logs, Logstash processes log data, and Kibana offers a user-friendly interface for log exploration.

## Repositories

### CyborgMessenger Backend
This is repository with backend application


**Link:** https://gitlab.com/portfolio133/cyborgsolutions/cyborgmessenger/cyborgmessenger_backend

### CyborgMessenger Frontend
This is repository with frontend application

**Link:** https://gitlab.com/portfolio133/cyborgsolutions/cyborgmessenger/cyborgmessenger_frontend